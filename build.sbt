import Dependencies._
import sbt.url

ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / version := Version.synaptixVersion
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"

val meta = """META.INF(.)*""".r

lazy val root = (project in file("."))
  .settings(
    name := "synaptix-jobs-controller",
    homepage := Some(url("http://www.mnemotix.com")),
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/mnemotix/synaptix/synaptix-jobs-controller.git"),
        "scm:git@gitlab.com:mnemotix/synaptix/synaptix-jobs-controller.git"
      )
    ),
    developers := List(
      Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lherisson",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "mrogelja",
        name = "Mathieu Rogelja",
        email = "mathieu.rogelja@mnemotix.com",
        url = url("http://www.mnemotix.com")
      )
    ),
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
    pomIncludeRepository := { _ => false },
    publishArtifact := true,
    publishMavenStyle := true,
    publishTo in ThisBuild := {
      val nexus = "https://nexus.mnemotix.com/repository"
      if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
      else Some("MNX Nexus" at nexus + "/maven-releases/")
    },
    resolvers ++= Seq(
      Resolver.mavenLocal,
      "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
      "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/",
//      Resolver.sonatypeRepo("public"),
      Resolver.typesafeRepo("releases"),
      Resolver.typesafeIvyRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      Resolver.bintrayRepo("owner", "repo")
    ),
    libraryDependencies ++= Seq(
      scalaTest % Test,
      akkaStreamTestkit % Test,
      logbackClassic,
      amqpToolkit,
      indexingToolkit,
      rdfToolkit,
      elasticAppender,
      wiktionnaireAnnotator,
      lifty,
      httpToolkit,
      cacheToolkit
    ),
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    updateOptions := updateOptions.value.withGigahorse(false),
    mainClass in(Compile, run) := Some("com.mnemotix.synaptix.jc.JobsController"),
    mainClass in assembly := Some("com.mnemotix.synaptix.jc.JobsController"),
    assemblyJarName in assembly := "synaptix-jobs-controller.jar",
    imageNames in docker := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/synaptix"),
        repository = name.value,
        tag = Some(version.value)
      )
    ),
    buildOptions in docker := BuildOptions(cache = false),
    dockerfile in docker := {
      // The assembly task generates a fat JAR file
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("java:jre-alpine")
        run("apk", "update")
        run("apk", "add", "bzip2")
        run("apk", "add", "zip")
        run("rm", "-rf" ,"/var/cache/apk/*")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )
  .enablePlugins(DockerPlugin)