import sbt._

object Version {
  lazy val scalaVersion = "2.12.7"
  lazy val scalaTest = "3.0.8"
  lazy val synaptixVersion = "0.1.8-SNAPSHOT"
  lazy val akkaVersion = "2.5.23"
  lazy val logback = "1.2.3"
  lazy val esAppender = "1.6"
  lazy val lifty = "0.1.2-SNAPSHOT"

}

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Version.akkaVersion
  lazy val amqpToolkit = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val indexingToolkit = "com.mnemotix" %% "synaptix-indexing-toolkit" % Version.synaptixVersion
  lazy val rdfToolkit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
  lazy val httpToolkit = "com.mnemotix" %% "synaptix-http-toolkit" % Version.synaptixVersion
  lazy val cacheToolkit = "com.mnemotix" %% "synaptix-cache-toolkit" % Version.synaptixVersion
  lazy val elasticAppender = "com.internetitem" % "logback-elasticsearch-appender" % Version.esAppender

  lazy val wiktionnaireAnnotator =  "org.ddf" %% "ddf-wiktionnaire-annotator" % Version.synaptixVersion
  lazy val lifty: ModuleID = "com.mnemotix" %% "lifty" % Version.lifty
}