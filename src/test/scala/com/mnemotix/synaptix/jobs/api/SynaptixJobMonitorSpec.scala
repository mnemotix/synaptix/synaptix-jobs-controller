package com.mnemotix.synaptix.jobs

import java.util.UUID

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.jobs.api.SynaptixJobMonitor
import play.api.libs.json.Json

import scala.concurrent.duration._

class SynaptixJobMonitorSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  "SynaptixJobMonitor" should {
    val monitor = new ConsoleMonitor

    "display log messages" in {
      monitor.refresh(LogMessage(UUID.randomUUID().toString, System.currentTimeMillis(), Json.obj("test" -> "test message")))
      monitor.isInstanceOf[SynaptixJobMonitor] shouldBe true
    }
  }
}
