package com.mnemotix.synaptix.jobs

import akka.Done
import akka.actor.Terminated
import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.jobs.api.JobsFixtures

import scala.concurrent.duration._


class SynaptixJobRunnerSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val source = JobsFixtures.words

  "SynaptixJobRunner" should {
    implicit val runner = new SynaptixJobRunner()
    runner.registerMonitor(new ConsoleMonitor)

    "run jobs and monitor their execution" in {
      val fj = new FakeJob()
      runner.registerJob(fj)
      runner.jobs.size shouldBe 1
      runner.run().futureValue shouldBe Done
      runner.unregisterJob(fj)
      runner.jobs.size shouldBe 0
    }

    "run long jobs and terminate their execution" in {
      val fj = new FakeInfiniteJob()
      runner.registerJob(fj)
      runner.jobs.size shouldBe 1
      runner.run().futureValue shouldBe Done

      runner.terminate(fj.jobId).futureValue shouldBe Terminated

      runner.unregisterJob(fj)
      runner.jobs.size shouldBe 0
    }
  }
}

