/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.jobs.api.JobsFixtures

import scala.collection.mutable
import scala.concurrent.duration._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-13
  */

class SynaptixChunkedDataSourceSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  val source = JobsFixtures.words

  "SynaptixChunkedDataSource" should {
    val iterator = new SynaptixChunkedDataSource[String]{
//      override val itemClass: String = "words"
      override val chunkSize: Int = 4
      override def count(): Int = source.size
      override def getChunk(offset: Int, size: Int): List[String] = source.slice(offset, offset + size).toList
    }
    "iterate over an input" in {
      iterator.count() shouldEqual 100
      iterator.nbChunks shouldEqual Math.ceil(source.size/iterator.chunkSize)
      val chunk = iterator.next()
      chunk.size shouldEqual iterator.chunkSize
      chunk.mkString(",") shouldEqual source.take(iterator.chunkSize).mkString(",")
      val collectedWords = mutable.ListBuffer[String]()
      collectedWords.append(chunk:_*)
      var iteration = 1
      while(iterator.hasNext){
        iteration+=1
        collectedWords.append(iterator.next():_*)
      }
      iteration shouldEqual iterator.nbChunks
      collectedWords.size shouldEqual source.size
    }
  }
}
