/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs.api

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-13
  */

object JobsFixtures {

  val words:Seq[String] = Seq(
    "lavish",
    "plantation",
    "invincible",
    "sofa",
    "relieved",
    "divergent",
    "destroy",
    "creator",
    "start",
    "present",
    "soothe",
    "sign",
    "bird",
    "trees",
    "limping",
    "cherry",
    "week",
    "political",
    "same",
    "impulse",
    "distinct",
    "legal",
    "babies",
    "damage",
    "decay",
    "cooperative",
    "cute",
    "kiss",
    "force",
    "geese",
    "sturdy",
    "legs",
    "striped",
    "unique",
    "anxious",
    "groan",
    "diligent",
    "spill",
    "dime",
    "seed",
    "twig",
    "fear",
    "weight",
    "beam",
    "ill",
    "entertaining",
    "pastoral",
    "expert",
    "mute",
    "yam",
    "noise",
    "zippy",
    "crack",
    "tongue",
    "vengeful",
    "inconclusive",
    "breakable",
    "brother",
    "trace",
    "nosy",
    "ducks",
    "tent",
    "treat",
    "weather",
    "plane",
    "suck",
    "spoon",
    "idiotic",
    "extra-large",
    "cub",
    "ceaseless",
    "flood",
    "illegal",
    "draconian",
    "endurable",
    "gentle",
    "skin",
    "terrible",
    "wide-eyed",
    "amazing",
    "bouncy",
    "decision",
    "used",
    "difficult",
    "lake",
    "womanly",
    "hateful",
    "cow",
    "turn",
    "love",
    "wiry",
    "children",
    "crow",
    "reign",
    "bare",
    "applaud",
    "cars",
    "late",
    "squealing",
    "suspect"
  )

}
