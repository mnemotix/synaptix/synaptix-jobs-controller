/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import akka.Done
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.synaptix.jobs.api.{JobsFixtures, SynaptixJob}
import play.api.libs.json.JsString

import scala.concurrent.Future

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-18
  */

class FakeInfiniteJob()(implicit val runner: SynaptixJobRunner) extends SynaptixJob {
  override val jobLabel: String = "Fake Job"
  override val jobDescription: Option[String] = None

  val src = Source(1 to 100000000).map{_ =>JobsFixtures.words}

  val flow = Flow[Seq[String]].map { list =>
    list.map(_.length).reduceLeft(_+_)
  }

  val output = Sink.foreach[Int]{i=>
    runner.refresh(LogMessage(jobId, System.currentTimeMillis(), JsString(s"$i letters counted")))
  }

  override val runnable: RunnableGraph[Future[Done]] = RunnableGraph.fromGraph(GraphDSL.create(output) {
    implicit builder => sink =>
        import GraphDSL.Implicits._
        src ~> flow ~> sink
        ClosedShape
  })

}
