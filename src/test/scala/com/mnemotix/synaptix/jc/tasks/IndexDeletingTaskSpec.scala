/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc.tasks

import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import com.mnemotix.synaptix.SynaptixTestSpec
import play.api.libs.json.{JsString, Json}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-07-24
  */

class IndexDeletingTaskSpec extends SynaptixTestSpec {
  override implicit val patienceConfig = PatienceConfig(Duration.Inf)
  "IndexDeletingTask" should {
    val task = new IndexDeletingTask("jobs.index.delete", AmqpClientConfiguration.exchangeName)
    "delete a given index" in {
      val message = AmqpMessage(Map.empty, JsString("ddf-20190724184218"))
      val resultMessage = Await.result(task.onMessage(message.toReadResult()), Duration.Inf)
      val msg = Json.parse(resultMessage.bytes.utf8String).as[AmqpMessage]
      msg.body shouldBe JsString("Done")
    }
  }
}
