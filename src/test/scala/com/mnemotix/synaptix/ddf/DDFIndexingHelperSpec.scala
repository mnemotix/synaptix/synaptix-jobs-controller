/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ddf

import com.mnemotix.synaptix.SynaptixTestSpec
import com.mnemotix.synaptix.core.utils.DateUtils
import com.mnemotix.synaptix.ddf.mappers.DDFEntryMapper
import com.mnemotix.synaptix.indexing.IndexingHelper
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.Json

import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-24
  */

class DDFIndexingHelperSpec extends SynaptixTestSpec {

  override implicit val patienceConfig = PatienceConfig(Duration.Inf)

  val repositoryName = "ddf"
  val indexName = repositoryName + s"-${DateUtils.getTimeStamp()}"
  implicit val conn = RDFClient.getReadConnection(Some(repositoryName))

  "IndexingHelper" should {
    "initialize its connections to RDF database and to index" in {
//            Try { GraphDbRestApiClient.createRepository(repositoryName, s"${repositoryName} Repository", RDFClientConfiguration.rootUri) }
      IndexingHelper.init()
    }

    "drop an index" in {
      Try {
        IndexingHelper.dropIndex(repositoryName).futureValue
      }
      IndexingHelper.indexExists(repositoryName).futureValue.result.exists shouldBe false
    }

    "create an index from an indexing model" in {
      val rs = IndexingHelper.getIndexingModel().futureValue.head
      IndexingHelper.createIndex(repositoryName, Json.parse(rs.getMapping())).futureValue.isSuccess shouldBe true
      IndexingHelper.indexExists(repositoryName).futureValue.result.exists shouldBe true
    }

    "get mappings from a given repository" in {
      val start = System.currentTimeMillis()
      val future = IndexingHelper.iterateOverClass(indexName, new DDFEntryMapper, 10)
//      val future = IndexingHelper.iterateOverClass(repositoryName, new DDFIndexMapper, 1000)
      future.onComplete{
        case Success(_) =>
        case Failure(error) => {
          logger.error("Graph indexing failed", error)
        }
      }
      future.futureValue
      val end = System.currentTimeMillis()
      println(s"Process took ${end-start} ms")
      IndexingHelper.shutdown()
    }
  }
}
