/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import scala.collection.mutable

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-13
  */

case class StepReport(itemProcessed:Int, stepMessage:Option[String], isError:Boolean=false)

case class SynaptixJobReport(itemType:String, nbItems:Int) {

  val stepReports = mutable.Buffer[StepReport]()

  def append(step:StepReport) = stepReports.append(step)
  def processed = stepReports.map(_.itemProcessed).reduceLeft(_+_)
  def completion:Int = ((processed.toDouble / nbItems.toDouble) * 100).toInt

  def progressMessage: String = s"$itemType [${processed}/${nbItems}]($completion%)"

  def progressBar(shorten:Boolean=false, messageOpt:Option[String]=None): String = {
    if (processed == nbItems) {
      return s"${Console.GREEN}Done.${Console.RESET}"
    }
    if (nbItems > 0) {
      val bar = new StringBuilder("[")
      for (i <- 0 to 50) {
        if (i < (completion / 2)) bar.append("=")
        else if (i == (completion / 2)) bar.append(">")
        else bar.append(" ")
      }
      bar.append(s"] ${completion.toString}%")
      return s"\r${bar.toString()}"
    }
    else return s"${Console.YELLOW}Nothing to process.${Console.RESET}"
  }
}
