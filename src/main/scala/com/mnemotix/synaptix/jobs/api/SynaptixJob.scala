/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.jobs.api

import java.util.UUID
import java.util.concurrent.Executors

import akka.Done
import akka.actor.ActorSystem
import akka.dispatch.ExecutionContexts
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.RunnableGraph
import com.mnemotix.synaptix.jobs.{LogMessage, SynaptixJobRunner}

import scala.concurrent.{ExecutionContext, Future}


/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-18
  */

trait SynaptixJob {

  val jobLabel: String
  val jobDescription: Option[String]
  val jobId: String = UUID.randomUUID().toString
  val threadPool: Option[Int] = SynaptixJobConfiguration.threadPool
  //  val subJobs = mutable.LinkedHashSet[SynaptixJob]()

  implicit val system: ActorSystem = ActorSystem(jobId)
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val executionContext: ExecutionContext = threadPool.map(nbThreads => new ExecutionContext {
    val threadPool = Executors.newFixedThreadPool(nbThreads)

    def execute(runnable: Runnable) {
      threadPool.submit(runnable)
    }

    def reportFailure(t: Throwable) {}
  }).getOrElse(ExecutionContexts.global())

  val runnable: RunnableGraph[Future[Done]]

  def run() = runnable.run()

  def refresh(message: LogMessage)(implicit runner: SynaptixJobRunner) = runner.refresh(message)

  def terminate() = system.terminate()

  def onTermination(callback: Unit) = system.registerOnTermination(callback)
}
