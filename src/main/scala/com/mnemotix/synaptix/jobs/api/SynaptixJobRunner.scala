/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import akka.Done
import akka.stream.Materializer
import com.mnemotix.synaptix.jobs.api.{SynaptixJob, SynaptixJobConfiguration, SynaptixJobMonitor}

import scala.collection.mutable
import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}



/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-18
  */

class SynaptixJobRunner() {
  val timeout:Duration = SynaptixJobConfiguration.globalTimeout.map(i => i.seconds).getOrElse(Duration.Inf)

  val jobs = mutable.LinkedHashSet[SynaptixJob]()
  val monitors = mutable.LinkedHashSet[SynaptixJobMonitor]()

  def registerJob(job:SynaptixJob) = jobs.add(job)
  def unregisterJob(job:SynaptixJob) = jobs.remove(job)

  def registerMonitor(monitor:SynaptixJobMonitor) = monitors.add(monitor)
  def unregisterMonitor(monitor:SynaptixJobMonitor) = monitors.remove(monitor)

  def refresh(message:LogMessage) = monitors.map(_.refresh(message))

  def terminate(jobId:String) = jobs.filter(_.jobId==jobId).head.terminate()

  def terminate()(implicit mat:Materializer, ec:ExecutionContext) = Future.sequence(jobs.map(_.terminate()).toSeq)

  def run()(implicit mat:Materializer, ec:ExecutionContext) = Future.sequence(jobs.map(_.run()).toSeq).transform {
      case Success(_) => Try{ Done.done() }
      case Failure(err) => throw JobRuntimeException("An exception occurred during jobs execution.", Some(err))
  }

  def runBlocking()(implicit mat:Materializer, ec:ExecutionContext) = Await.result(run(), timeout)
}