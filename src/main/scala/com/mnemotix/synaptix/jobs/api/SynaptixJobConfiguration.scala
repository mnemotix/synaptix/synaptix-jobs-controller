/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mnemotix.synaptix.jobs.api

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-18
  */

object SynaptixJobConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("jobs")).getOrElse(ConfigFactory.empty())
  lazy val threadPool: Option[Int] = if(conf.hasPath("thread.pool")) Some(conf.getInt("global.timeout")) else None
  lazy val globalTimeout: Option[Int] = if(conf.hasPath("global.timeout")) Some(conf.getInt("global.timeout")) else None
}
