/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jobs

import play.api.libs.json.{JsObject, JsValue, Json, Writes}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-18
  */

case class LogMessage(senderId: String, creationDate: Long, message: JsValue) {
  def toJson()(implicit writes:Writes[LogMessage]) = Json.toJson(this).as[JsObject]

  def prettyPrint()(implicit writes:Writes[LogMessage]) = Json.prettyPrint(toJson())
  override def toString() = Json.stringify(toJson()(LogMessage.format))
}

object LogMessage {
  implicit lazy val format = Json.format[LogMessage]
}
