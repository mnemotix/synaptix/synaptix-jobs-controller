/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ddf.mappers

import java.io.ByteArrayOutputStream

import com.mnemotix.synaptix.ddf.models._
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.mnemotix.synaptix.indexing.{IndexMapper, JSONLDMapper}
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.{RDFDataMgr, RDFFormat}
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-27
  */

class DDFEntryMapper extends IndexMapper {

  override def map(model: Model)(implicit ec: ExecutionContext): Future[ESIndexable] = Future {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    RDFDataMgr.write(os, model, RDFFormat.JSONLD)
    val str = new String(os.toByteArray, "UTF-8")
    os.close()
    val e = parse(Json.parse(str))
    toIndexable(Json.toJson(e).as[JsObject])
  }

  def parse(jsonld: JsValue): Option[Entry] = {
    try {
      val jsm = new JSONLDMapper(jsonld)

      def getLexicalEntries(jsval: Option[JsValue]) =
        jsval.map(jsm.getUris(_).flatMap(jsm.getValueById(_).flatMap(_.asOpt[LexicalEntry]).map { lexentry =>
          lexentry.senses = getSenses(lexentry.sense)
          lexentry.canonical = jsm.mapValue[CanonicalForm](lexentry.canonicalForm)
          lexentry.partOfSpeech = jsm.mapValue[LocalizedLabel](lexentry.pos)
          lexentry.gendr = jsm.mapValue[LocalizedLabel](lexentry.gender)
          lexentry.others = jsm.mapValues[LocalizedLabel](lexentry.otherForm)
          lexentry
        }))

      def getSenses(jsval: Option[JsValue]) = jsm.mapValues[Sense](jsval).map(_.map { sense =>
        sense.usageExamples = jsm.mapValues[UsageExample](sense.usageExample)
        sense
      })

      val jsentry = jsm.nodes.find(jsval => (jsval \ "@type").as[String] == "lexicog:Entry")
      val root: Option[Entry] = jsentry.flatMap(_.asOpt[Entry]).map { entry =>
        entry.lentries = getLexicalEntries(entry.lentry)
        entry
      }
      root
    }
    catch {
      case jse: JsResultException => {
        println(Json.prettyPrint(jsonld))
        logger.error("Parsing error", jse)
        None
      }
      case t: Throwable => {
        logger.error("Unknown error", t)
        None
      }
    }
  }


  def toIndexable(doc: JsObject):ESIndexable = {
    val src = Json.obj(
      "@graph" -> (doc \ "graph").get,
      "@type" -> (doc \ "@type").get,
      "lentries" -> (doc \ "lentries").get
    )
    ESIndexable((doc \ "@id").as[String], "entry", Some(src))
  }

}
