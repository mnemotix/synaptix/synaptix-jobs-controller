/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ddf.mappers

import java.io.ByteArrayOutputStream

import com.mnemotix.synaptix.ddf.models.{DDFForm, SimpleRef}
import com.mnemotix.synaptix.index.elasticsearch.models.ESIndexable
import com.mnemotix.synaptix.indexing.{IndexMapper, JSONLDMapper}
import com.mnemotix.synaptix.jobs.IndexingJobException
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.{RDFDataMgr, RDFFormat}
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-10-03
  */

class DDFFormMapper extends IndexMapper {

  override def map(model: Model)(implicit ec: ExecutionContext): Future[ESIndexable] = Future {
    val os: ByteArrayOutputStream = new ByteArrayOutputStream()
    RDFDataMgr.write(os, model, RDFFormat.JSONLD)
    val jsonld = new String(os.toByteArray, "UTF-8")
    os.close()
    val entity:Option[DDFForm] = parse(Json.parse(jsonld))
    val parsing = Json.toJson(entity).validate[JsObject]
    if(parsing.isSuccess){
      val jsform = toIndexable(Json.toJson(entity).as[JsObject])
      jsform
    } else {
      logger.error("Unable to map the result")
      throw new IndexingJobException("Unable to map the result: "+ entity, None)
    }
  }

  def parse(jsonld: JsValue): Option[DDFForm] = {
    try {
      val jsm = new JSONLDMapper(jsonld)
      val jsform:Option[JsValue] = jsm.nodes.find{ jsval =>
        val parsing = (jsval \ "@type").validate[String]
        if(parsing.isSuccess && parsing.get == "ontolex:Form") true else false
      }
      val root: Option[DDFForm] = jsform.flatMap(_.asOpt[DDFForm]).map { form =>
        form.canonicalFormOf = jsm.mapValue[SimpleRef](form.canonicalFormOfLentry)
        form.otherFormOf = jsm.mapValue[SimpleRef](form.otherFormOfLentry)
        form
      }
      if(root.isEmpty) {
        logger.error(Json.prettyPrint(jsonld))
      }
      root
    }
    catch {
      case jse: JsResultException => {
        logger.error("Parsing error", jse)
        None
      }
      case t: Throwable => {
        logger.error("Unknown error", t)
        None
      }
    }
  }

  def toIndexable(doc: JsObject): ESIndexable = {
    val src = Json.obj(
      "@graph" -> (doc \ "graph").getOrElse(null),
      "@type" -> (doc \ "@type").get,
      "locations" -> (doc \ "locations").getOrElse(null),
      "label" -> (doc \ "label").getOrElse(null),
      "lang" -> (doc \ "lang").getOrElse(null),
      "phonetic" -> (doc \ "phonetic").getOrElse(null),
      "canonicalFormOf" -> (doc \ "canonicalFormOf").getOrElse(null),
      "otherFormOf" -> (doc \ "otherFormOf").getOrElse(null)
    )
    ESIndexable((doc \ "@id").as[String], "form", Some(src))
//    ESIndexable((doc \ "@id").as[String], "form", None)
  }

}
