/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.ddf.models

import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.{JsValue, Json, _}

import scala.collection.mutable // Combinator syntax
/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-07-02
  */

case class Entry(`@id`: String, `@type`: JsValue, lentry: Option[JsValue], graph: Option[String], var lentries: Option[Seq[LexicalEntry]] = None)

case class LexicalEntry(
  `@id`: String,
  `@type`: Option[JsValue] = Some(JsString("ontolex:LexicalEntry")),
  sense: Option[JsValue] = None,
  canonicalForm: Option[JsValue] = None,
  gender: Option[JsValue] = None,
  pos: Option[JsValue] = None,
  otherForm: Option[JsValue] = None,
  etymology: Option[JsValue] = None,
  var senses: Option[Seq[Sense]] = None,
  var canonical: Option[CanonicalForm] = None,
  var gendr: Option[LocalizedLabel] = None,
  var others: Option[Seq[LocalizedLabel]] = None,
  var partOfSpeech: Option[LocalizedLabel] = None
)

case class Sense(`@id`: String, `@type`: Option[JsValue] = None, definition: Option[LocalizedLabel] = None, locations: Option[JsValue] = None, usageExample: Option[JsValue] = None, var usageExamples: Option[Seq[UsageExample]] = None)

case class CanonicalForm(`@id`: String, locations: Option[JsValue], label: String, lang: String, phoneticRep: Option[String])

case class UsageExample(`@id`: String, `@type`: Option[JsValue] = None, bibliographicalCitation: Option[LocalizedLabel], value: Option[LocalizedLabel])

case class LocalizedLabel(`@id`: Option[String], `@type`: Option[JsValue], label: String, lang: String)

case class SimpleRef(`@id`: String, `@type`: Option[JsValue] = None)

object SimpleRef {
  implicit lazy val reads = Json.reads[SimpleRef]
  implicit lazy val writes = new Writes[SimpleRef] {
    def writes(sr: SimpleRef): JsObject = {
      Json.obj(
        "@id" -> sr.`@id`,
        "@type" -> sr.`@type`
      )
    }
  }
}

object LocalizedLabel {

  implicit val reads: Reads[LocalizedLabel] = (
    (JsPath \ "@id").readNullable[String] and
      (JsPath \ "@type").readNullable[JsValue] and
      ((JsPath \ "label").read[String] or (JsPath \ "@value").read[String]) and
      ((JsPath \ "lang").read[String] or (JsPath \ "@language").read[String])
    ) (LocalizedLabel.apply _)

  implicit lazy val writes = new Writes[LocalizedLabel] {
    def writes(loclabel: LocalizedLabel): JsObject = {
      val fields = mutable.Buffer[(String, JsValue)]()
      if (loclabel.`@id`.isDefined) fields.append("id" -> JsString(loclabel.`@id`.get))
      if (loclabel.`@type`.isDefined) fields.append("type" -> loclabel.`@type`.get)
      fields.append(
        "label" -> JsString(loclabel.label),
        "lang" -> JsString(loclabel.lang)
      )
      JsObject(fields)
    }
  }

}

object CanonicalForm {
  implicit lazy val reads = Json.reads[CanonicalForm]
  implicit lazy val writes = new Writes[CanonicalForm] {
    def writes(form: CanonicalForm): JsObject = {
      Json.obj(
        "@id" -> form.`@id`,
        "locations" -> form.locations,
        "label" -> form.label,
        "lang" -> form.lang,
        "phonetic" -> form.phoneticRep
      )
    }
  }

}

object UsageExample {
  implicit lazy val reads = Json.reads[UsageExample]
  implicit lazy val writes = new Writes[UsageExample] {
    def writes(ue: UsageExample): JsObject = {
      Json.obj(
        "@id" -> ue.`@id`,
        "@type" -> ue.`@type`,
        "bibliographicalCitation" -> Json.toJson(ue.bibliographicalCitation),
        "value" -> Json.toJson(ue.value)
      )
    }
  }

}

object Sense {

  implicit lazy val reads = Json.reads[Sense]

  implicit lazy val writes = new Writes[Sense] {
    def writes(sense: Sense): JsObject = {
      Json.obj(
        "@id" -> sense.`@id`,
        "@type" -> sense.`@type`,
        "definition" -> Json.toJson(sense.definition),
        "locations" -> sense.locations,
        "usageExamples" -> Json.toJson(sense.usageExamples)
      )
    }
  }

}

object LexicalEntry {

  implicit lazy val reads = Json.reads[LexicalEntry]

  implicit lazy val writes = new Writes[LexicalEntry] {
    def writes(lexentry: LexicalEntry): JsObject = {
      Json.obj(
        "@id" -> lexentry.`@id`,
        "@type" -> lexentry.`@type`,
        "senses" -> Json.toJson(lexentry.senses),
        "canonicalForm" -> Json.toJson(lexentry.canonical),
        "gender" -> Json.toJson(lexentry.gendr),
        "otherForms" -> Json.toJson(lexentry.others),
        "partOfSpeech" -> Json.toJson(lexentry.partOfSpeech))
    }
  }
}

object Entry {
  implicit lazy val reads = Json.reads[Entry]

  implicit lazy val writes = new Writes[Entry] {
    def writes(entry: Entry): JsObject = {
      Json.obj(
        "@id" -> entry.`@id`,
        "@type" -> entry.`@type`,
        "graph" -> entry.graph,
        "lentries" -> Json.toJson(entry.lentries.get)
      )
    }
  }
}

case class DDFForm(
  `@id`: String,
  `@type`: JsValue,
  graph: Option[String] = None,
  var canonicalFormOf: Option[SimpleRef] = None,
  var otherFormOf: Option[SimpleRef] = None,
  canonicalFormOfLentry: Option[JsValue] = None,
  otherFormOfLentry: Option[JsValue] = None,
  locations: Option[JsValue] = None,
  phonetic: Option[String] = None,
  label: Option[String] = None,
  lang: Option[String] = None
)

object DDFForm {
  implicit lazy val reads = Json.reads[DDFForm]

  implicit lazy val writes = new Writes[DDFForm] {
    def writes(entity: DDFForm): JsObject = {
      Json.obj(
        "@id" -> entity.`@id`,
        "@type" -> entity.`@type`,
        "graph" -> entity.graph,
        "canonicalFormOf" -> entity.canonicalFormOf,
        "otherFormOf" -> entity.otherFormOf,
        "locations" -> entity.locations,
        "phonetic" -> entity.phonetic,
        "label" -> entity.label,
        "lang" -> entity.lang
      )
    }
  }
}
