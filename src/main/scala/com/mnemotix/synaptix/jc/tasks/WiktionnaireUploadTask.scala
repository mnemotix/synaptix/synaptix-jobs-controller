package com.mnemotix.synaptix.jc.tasks

import java.io.File

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import akka.util.ByteString
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import com.mnemotix.synaptix.cache.RocksDBStore
import org.ddf.wiktionnaire.annotator.helpers.FileSystemToolKit
import org.ddf.wiktionnaire.annotator.services.{WiktionnaireAMQPKit, WiktionnaireRdfFileUploader}
import play.api.libs.json.{JsString, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireUploadTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {
    val jsval = Json.parse(msg.bytes.utf8String)
    jsval.validate[AmqpMessage].isSuccess match {
      case true => {
        val wiktionnaireAMQPKit = new WiktionnaireAMQPKit()
        val wiktionnaireRdfFileUploader = new WiktionnaireRdfFileUploader()
        wiktionnaireRdfFileUploader.init

        wiktionnaireAMQPKit.taskMessageProgress("Téléversement des triplets générés", "0", "3")

        val json = jsval.as[AmqpMessage]
        val fileLoc = json.body.asOpt[String]
        val futureLock = FileSystemToolKit.gzipFiles(fileLoc.get)
        futureLock.onComplete {
          case Success(value) => logger.info(s"${value.size} files was zip")
          case Failure(e) => e.printStackTrace
        }
        Await.result(futureLock, Duration.Inf)
        val gzipFiles = FileSystemToolKit.getListOfFiles(new File(fileLoc.get), List("gzip"))

        wiktionnaireAMQPKit.taskMessageProgress("Téléversement des triplets générés", "50", "3")

        val futureClose = Future.sequence(wiktionnaireRdfFileUploader.loadRdfFiles(gzipFiles))
        futureClose.transform {
          case Success(_) => Try {
            wiktionnaireAMQPKit.taskMessageProgress("Téléversement des triplets générés", "100", "3")
            getResponseMessage(JsString(s"Done"), "JSON", "OK", msg.properties)
          }
          case Failure(err) => Try {
            logger.error("upload failed", err)
            getErrorMessage(err, msg.properties)
          }
        }
      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        Future(
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        )
      }
    }
  }
}