/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc.tasks

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import akka.util.ByteString
import com.mnemotix.amqp.api.{AmqpClientConfiguration, AmqpMessage}
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import com.mnemotix.synaptix.cache.RocksDBStore
import org.ddf.wiktionnaire.annotator.services.{WiktionnaireFileOutput, WiktionnaireHTMLCrawler}
import play.api.libs.json.{JsString, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


/**
  * Created by Pierre-René LHERISSON (pr.lherisson@mnemotix.com).
  * Date: 2019-09-12
  */

class GraphWiktionnaireCreatingTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {
    val jsval = Json.parse(msg.bytes.utf8String)
    jsval.validate[AmqpMessage].isSuccess match {
      case true => {
        val wiktionnaireCreatingTaskCache = new RocksDBStore("creatingTaskCache")
        implicit val htmlCrawler = new WiktionnaireHTMLCrawler()
        lazy val wiktionnaireFileOutput = new WiktionnaireFileOutput()
        wiktionnaireCreatingTaskCache.init
        val futLaunched = wiktionnaireCreatingTaskCache.get("launched")
        val futLastDate = wiktionnaireCreatingTaskCache.get(wiktionnaireFileOutput.dir)
        val launched = Await.result(futLaunched, Duration.Inf)
        val lastDate =  Await.result(futLastDate, Duration.Inf)

        if (!launched.isDefined && !lastDate.isDefined) {
          Await.result(wiktionnaireCreatingTaskCache.put("launched", ByteString("1")), Duration.Inf)
          val initWiktionnaireTask = new WiktionnaireGraphDBinitTask("jobs.init.wiktionnaire", AmqpClientConfiguration.exchangeName)
          val downloadTask = new WiktionnaireDownloadTask("jobs.download.wiktionnaire", AmqpClientConfiguration.exchangeName)
          val annotateTask = new WiktionnaireAnnotateTask("jobs.annotate.wiktionnaire", AmqpClientConfiguration.exchangeName)
          val uploadTask = new WiktionnaireUploadTask("jobs.upload.wiktionnaire", AmqpClientConfiguration.exchangeName)

          val initMessage = AmqpMessage(Map.empty, JsString(""))
          val resultMessage = Await.result(initWiktionnaireTask.onMessage(initMessage.toReadResult()), Duration.Inf)

          val fileLink = "https://dumps.wikimedia.org/frwiktionary/latest/frwiktionary-latest-pages-articles.xml.bz2"
          val downloadMessage = AmqpMessage(Map.empty, JsString(fileLink))
          val downloadResultMessage = Await.result(downloadTask.onMessage(downloadMessage.toReadResult()), Duration.Inf)
          val downloadMsg = Json.parse(downloadResultMessage.bytes.utf8String).as[AmqpMessage]

          val annotateMessage = AmqpMessage(Map.empty, downloadMsg.body)
          val annotateResultMessage = Await.result(annotateTask.onMessage(annotateMessage.toReadResult()), Duration.Inf)
          val annotateMsg = Json.parse(annotateResultMessage.bytes.utf8String).as[AmqpMessage]

          val uploadMessage = AmqpMessage(Map.empty,annotateMsg.body)
           uploadTask.onMessage(uploadMessage.toReadResult()).transform {
            case Success(message) => Try {
              Await.result(wiktionnaireCreatingTaskCache.delete("launched"), Duration.Inf)
              val uploadMsg = Json.parse(message.bytes.utf8String).as[AmqpMessage]
              getResponseMessage(uploadMsg.body, "JSON", "OK", msg.properties)
            }
            case Failure(err) => Try {
              Await.result(wiktionnaireCreatingTaskCache.delete("launched"), Duration.Inf)
              logger.error("Wiktionnaire annotator failed", err)
              getErrorMessage(err, msg.properties)
            }
          }
        }
        else {
          Future(getResponseMessage(JsString(s"The process is already launched or Annotations already exist"), "JSON", "OK", msg.properties))
        }
      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        Future(
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        )
      }
    }
  }
}