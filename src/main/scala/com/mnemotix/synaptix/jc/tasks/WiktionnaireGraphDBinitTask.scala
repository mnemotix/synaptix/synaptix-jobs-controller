package com.mnemotix.synaptix.jc.tasks

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import org.ddf.wiktionnaire.annotator.services.{WiktionnaireAMQPKit, WiktionnaireRdfFileUploader}
import play.api.libs.json.{JsString, Json}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class WiktionnaireGraphDBinitTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {
  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {
    val jsval = Json.parse(msg.bytes.utf8String)
    jsval.validate[AmqpMessage].isSuccess match {
      case true => {
        val wiktionnaireAMQPKit = new WiktionnaireAMQPKit()
        wiktionnaireAMQPKit.taskMessageProgress("Préparation de la base de données", "0", "1")

        val wiktionnaireRdfFileUploader = new WiktionnaireRdfFileUploader()
        wiktionnaireRdfFileUploader.init
        wiktionnaireRdfFileUploader.initRepository.transform {
          case Success(_) => Try {
            getResponseMessage(JsString(s"GraphDB repository is ready"), "JSON", "OK", msg.properties)
          }
          case Failure(err) => Try {
            logger.error("GraphDB init failed", err)
            getErrorMessage(err, msg.properties)
          }
        }

      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        Future(
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        )
      }
    }
  }
}
