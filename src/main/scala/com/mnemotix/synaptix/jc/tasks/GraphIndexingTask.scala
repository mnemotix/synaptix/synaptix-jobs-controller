/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc.tasks

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-21
  */

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.{ReadResult, WriteMessage}
import com.mnemotix.amqp.api.AmqpMessage
import com.mnemotix.amqp.api.rpc.AmqpRpcTask
import com.mnemotix.exceptions.MessageParsingException
import com.mnemotix.synaptix.core.utils.DateUtils
import com.mnemotix.synaptix.ddf.mappers.{DDFEntryMapper, DDFFormMapper}
import com.mnemotix.synaptix.indexing.{IndexingHelper, IndexingModel}
import com.mnemotix.synaptix.jobs.IndexingJobException
import com.mnemotix.synaptix.rdf.client.RDFClient
import play.api.libs.json.{JsString, Json}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-03-20
  */

class GraphIndexingTask(override val topic: String, override val exchangeName: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends AmqpRpcTask {

  override def onMessage(msg: ReadResult, params: String*)(implicit ec: ExecutionContext): Future[WriteMessage] = {

    val jsval = Json.parse(msg.bytes.utf8String)
    jsval.validate[AmqpMessage].isSuccess match {
      case true => {
        val json = jsval.as[AmqpMessage]
        val time = DateUtils.getTimeStamp()
        val repositoryName = json.body.asOpt[String]
//        val repo = json.headers.get("repository").map(_.as[String])
        implicit val conn = RDFClient.getReadConnection(repositoryName)

        // Wait for the cluster to be online
//        IndexingHelper.init()

        IndexingHelper.registerMapper("http://www.w3.org/ns/lemon/lexicog#Entry", new DDFEntryMapper)
        IndexingHelper.registerMapper("http://www.w3.org/ns/lemon/ontolex#Form", new DDFFormMapper)

        // Recreate index from indexing model
        val futureImodels: Future[Seq[IndexingModel]] = IndexingHelper.getIndexingModel()
        val imodels = Await.result(futureImodels, Duration.Inf)

        val start = System.currentTimeMillis()

        val overallFuture = Future {
          imodels.foreach { im =>
            val docType = im.clazz.split("#").last.toLowerCase
            val aliasName = repositoryName + s"-$docType"
            val indexName = s"$aliasName-$time"
            Try { Await.result(IndexingHelper.removeAlias(s"$aliasName-*", aliasName), Duration.Inf)}
            Try { Await.result(IndexingHelper.createIndex(indexName, Json.parse(im.getMapping())), Duration.Inf)}
            val futureOpt = IndexingHelper.processIndexingModel(indexName, im, 10)
            futureOpt.map { future =>
              future.onComplete {
                case Success(_) => {
                  Await.result(IndexingHelper.addAlias(indexName, aliasName), Duration.Inf)
                  println(s"Alias $repositoryName created on index ${indexName}.")
                }
                case Failure(err) => {
                  throw new IndexingJobException("Graph indexing failed", Some(err))
                }
              }
              Await.result(future, Duration.Inf)
            }
          }
        }

        overallFuture.transform {
          case Success(_) => Try {
            val end = System.currentTimeMillis()
            println(s"Process took ${end - start} ms")
            getResponseMessage(JsString("Done"), "JSON", "OK", msg.properties)
          }
          case Failure(err) => Try {
            logger.error("Graph indexing failed", err)
            getErrorMessage(err, msg.properties)
          }
        }
      }
      case false => {
        logger.error(s"Unable to parse the message: ${msg.bytes.utf8String}")
        Future(
          getErrorMessage(new MessageParsingException(s"Unable to parse the message: ${msg.bytes.utf8String}", null), msg.properties)
        )
      }
    }
  }

}
