/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.jc

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.rpc.AmqpRpcController
import com.mnemotix.synaptix.indexing.IndexingHelper
import com.mnemotix.synaptix.jc.tasks.{GraphIndexingTask, GraphWiktionnaireCreatingTask, IndexDeletingTask, WiktionnaireAnnotateTask, WiktionnaireDownloadTask, WiktionnaireGraphDBinitTask, WiktionnaireUploadTask}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-21
  */

object JobsController extends App  with LazyLogging {

  implicit val system = ActorSystem("JobsControllerSystem")
  implicit val materializer = ActorMaterializer()
  implicit val ec: ExecutionContext = system.dispatcher

  logger.info("Jobs controller starting...")

  IndexingHelper.init()

  val controller = new AmqpRpcController("Jobs Controller")
  controller.registerTask("jobs.graph.index", new GraphIndexingTask("jobs.graph.index", AmqpClientConfiguration.exchangeName))
  controller.registerTask("datasource.load.wiktionnaire", new GraphWiktionnaireCreatingTask("datasource.load.wiktionnaire", AmqpClientConfiguration.exchangeName))
  controller.registerTask("jobs.index.delete", new IndexDeletingTask("jobs.index.delete", AmqpClientConfiguration.exchangeName))

  //wiktionnaire subtasks
  controller.registerTask("jobs.init.wiktionnaire", new WiktionnaireGraphDBinitTask("jobs.init.wiktionnaire", AmqpClientConfiguration.exchangeName))
  controller.registerTask("jobs.download.wiktionnaire", new WiktionnaireDownloadTask("jobs.download.wiktionnaire", AmqpClientConfiguration.exchangeName))
  controller.registerTask("jobs.annotate.wiktionnaire", new WiktionnaireAnnotateTask("jobs.annotate.wiktionnaire", AmqpClientConfiguration.exchangeName))
  controller.registerTask("jobs.upload.wiktionnaire", new WiktionnaireUploadTask("jobs.upload.wiktionnaire", AmqpClientConfiguration.exchangeName))

  val starting = controller.start

  logger.info("Jobs controller started successfully.")

  sys addShutdownHook {
    Await.result(controller.shutdown, Duration.Inf)
  }

}
