/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.indexing

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESIndexable, ESMappingDefinitions}
import com.mnemotix.synaptix.rdf.client.models.{RDFClientReadConnection, RDFModel}
import com.mnemotix.synaptix.rdf.client.{RDFClient, RDFClientConfiguration}
import com.sksamuel.elastic4s.http.Response
import com.sksamuel.elastic4s.http.cat.CatIndicesResponse
import com.sksamuel.elastic4s.http.index.CreateIndexResponse
import com.sksamuel.elastic4s.mappings.MappingDefinition
import com.typesafe.scalalogging.LazyLogging
import org.apache.jena.query.{QuerySolution, ResultSet}
import org.apache.jena.rdf.model.Model
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-06-24
  */

object IndexingHelper extends LazyLogging {

  val mappers = mutable.HashMap[String, IndexMapper]()

  def init()(implicit ec: ExecutionContext) = {
    RDFClient.init()
    IndexClient.init()
  }

  def shutdown()(implicit ec: ExecutionContext) = {
    RDFClient.close()
    IndexClient.shutdown()
  }

  def registerMapper(clazz: String, mapper: IndexMapper) = mappers.put(clazz, mapper)

  def mute() = {
    RDFClient.mute()
    IndexClient.mute()
  }

  def verbose() = {
    RDFClient.verbose()
    IndexClient.verbose()
  }

  def dropIndex(indexName: String)(implicit ec: ExecutionContext) = IndexClient.deleteIndex(indexName)

  def indexExists(indexName: String)(implicit ec: ExecutionContext) = IndexClient.indexExists(indexName)

  def createIndex(indexName: String, json: JsValue)(implicit ec: ExecutionContext) = {
    val mappings = ESMappingDefinitions(Json.obj(
      "mappings" -> json.as[JsObject]
    ))
    val future = IndexClient.createIndex(indexName, mappings.toMappingDefinitions(): _*)
    future.onComplete {
      case Success(resp) => logger.info(resp.body.getOrElse(""))
      case Failure(err) => logger.error(err.getMessage, err)
    }
    future
  }

  def createIndex(indexName: String, mapping: MappingDefinition)(implicit ec: ExecutionContext): Future[Response[CreateIndexResponse]] = IndexClient.createIndex(indexName, mapping)

  def addAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext) = IndexClient.addAlias(indexName, aliasName)

//  def removeAliases(indexNamePattern: String, aliasName: String)(implicit ec: ExecutionContext) = {
//    val indices = Await.result(IndexClient.listIndices(), Duration.Inf).result.map {
//      res:CatIndicesResponse =>
//        val indexName = res.
//        IndexClient.removeAlias(indexName, aliasName)
//    }
//  }

  def removeAlias(indexName: String, aliasName: String)(implicit ec: ExecutionContext) = IndexClient.removeAlias(indexName, aliasName)

  def bulkInsert(indexName: String, indexables: ESIndexable*)(implicit ec: ExecutionContext) = IndexClient.bulkInsert(indexName, indexables: _*)

  def insertDoc(indexName: String, indexable: ESIndexable)(implicit ec: ExecutionContext) = IndexClient.insert(indexName, indexable)

  def getIndexingModel()(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[Seq[IndexingModel]] = {
    val qry =
      """
        |PREFIX im: <http://ns.mnemotix.com/ontologies/2019/1/indexing-model#>
        |
        |SELECT DISTINCT * WHERE {
        |    GRAPH ?g {
        |	    ?im a im:IndexingModel ; im:indexingModelOf ?class; im:mapping ?mapping ; im:query ?query ; im:template ?template
        |    }
        |}
      """.stripMargin
    val models = RDFClient.select(qry)(ec, conn).map { rs =>
      rs.get[ResultSet].asScala.map { qs =>
        val g: String = qs.getResource("?g").getURI
        val uri: String = qs.getResource("?im").getURI
        val clazz: String = qs.getResource("?class").getURI
        val mapping: String = qs.getLiteral("?mapping").getString
        val query: String = qs.getLiteral("?query").getString
        val template: String = qs.getLiteral("?template").getString
        IndexingModel(uri, g, clazz, mapping, query, template)
      }.toSeq
    }
    models
  }

  def countByClass(clazz: String)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): Future[Int] = {
    val qry = s"""SELECT (count(distinct ?uri) as ?cnt) WHERE { ?uri a <$clazz> }"""
    RDFClient.select(qry).map { rs =>
      rs.get[ResultSet].next().get("?cnt").asLiteral().getInt
    }
  }

  def getInstances(clazz: String, offset: Int, limit: Int): String =
    s"""
       |SELECT DISTINCT ?uri WHERE { ?uri a <$clazz> }
       |ORDER BY ?uri
       |LIMIT $limit
       |OFFSET $offset
      """.stripMargin

  def getMapperByClass(str: String): Option[IndexMapper] = {
    mappers.get(str)
  }

  def processIndexingModel(indexName: String, im: IndexingModel, chunkSize: Int)(implicit ec: ExecutionContext, conn: RDFClientReadConnection, mat: Materializer) = {
    val count = Await.result(countByClass(im.clazz), Duration.Inf)
    val mapperOpt: Option[IndexMapper] = getMapperByClass(im.clazz)
    if (mapperOpt.isEmpty) logger.warn(s"No mapper war found for class: ${im.clazz}. Indexing model was skipped.")
    mapperOpt.map { mapper =>
      val src: Source[QuerySolution, NotUsed] = Source.fromIterator(() => getIterator(im.clazz, chunkSize, count))
        .flatMapConcat(futureSeq => Source.fromFuture(futureSeq))
        .flatMapConcat(rs => Source.fromIterator(() => rs.get[ResultSet].asScala))

      var processed = 0

      val f1 = Flow[QuerySolution].mapAsync(200) { qs =>
        im.indexingQuery(qs)
      }

      val f2 = Flow[RDFModel].mapAsync(200) { model =>
        mapper.map(model.get[Model])
      }

      val f3 = Flow[ESIndexable].map { indexable =>
        insertDoc(indexName, indexable)
        1
      }

      val f4 = Flow[Int].map { i =>
        processed += i
        progress(processed, count)
      }

      mute()

      val f = src.via(f1).via(f2).via(f3).via(f4).runWith(Sink.ignore)

      f.onComplete {
        case Success(_) => {
          verbose()
          logger.info("Indexing done.")
        }
        case Failure(error) => {
          RDFClient.verbose()
          logger.error("Indexing failed.", error)
        }
      }
      f
    }
  }


  def iterateOverClass(indexName: String, mapper: IndexMapper, chunkSize: Int)(implicit ec: ExecutionContext, conn: RDFClientReadConnection, mat: Materializer) = {
    val im = Await.result(getIndexingModel(), Duration.Inf).head
    val count = Await.result(countByClass(im.clazz), Duration.Inf)

    val src: Source[QuerySolution, NotUsed] = Source.fromIterator(() => getIterator(im.clazz, chunkSize, count))
      .flatMapConcat(futureSeq => Source.fromFuture(futureSeq))
      .flatMapConcat(rs => Source.fromIterator(() => rs.get[ResultSet].asScala))

    var processed = 0

    val f1 = Flow[QuerySolution].mapAsync(200) { qs =>
      im.indexingQuery(qs)
    }

    val f2 = Flow[RDFModel].mapAsync(200) { model =>
      mapper.map(model.get[Model])
    }

    val f3 = Flow[ESIndexable].map { indexable =>
      insertDoc(indexName, indexable)
      1
    }

    val f4 = Flow[Int].map { i =>
      processed += i
      progress(processed, count)
    }

    mute()

    val f = src.via(f1).via(f2).via(f3).via(f4).runWith(Sink.ignore)

    f.onComplete {
      case Success(_) => {
        verbose()
        logger.info("Indexing done.")
      }
      case Failure(error) => {
        RDFClient.verbose()
        logger.error("Indexing failed.", error)
      }
    }
    f
  }

  def getIterator(clazz: String, chunkSize: Int, nbItems: Int)(implicit ec: ExecutionContext, conn: RDFClientReadConnection): SparqlClassChunkedIterator = new SparqlClassChunkedIterator(
    getInstances,
    clazz,
    chunkSize,
    nbItems
  )

  def progress(processed: Int, grandTotal: Int) = {
    if (grandTotal > 0) {
      val progress = processed
      val percent = ((progress.toDouble / grandTotal.toDouble) * 100).toInt
      val bar = new StringBuilder("[")
      for (i <- 0 to 50) {
        if (i < (percent / 2)) bar.append("=")
        else if (i == (percent / 2)) bar.append(">")
        else bar.append(" ")
      }
      bar.append(s"] ${percent.toString}%")
      println(bar.toString())
      //      print("\r" + bar.toString())
      if (progress == grandTotal) {
        println(Console.GREEN + "\nDone." + Console.RESET)
      }
    }
  }

}
