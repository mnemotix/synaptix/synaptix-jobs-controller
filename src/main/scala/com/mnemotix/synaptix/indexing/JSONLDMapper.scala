/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.mnemotix.synaptix.indexing

import com.typesafe.scalalogging.LazyLogging
import play.api.libs.json._

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-10-07
  */

class JSONLDMapper(jsonld: JsValue) extends LazyLogging {

  val nodes: IndexedSeq[JsValue] = (jsonld \ "@graph").as[JsArray].value

  val context = (jsonld \ "@context").as[JsObject].value

  val byId: Map[String, JsValue] = nodes.map { node =>
    (node \ "@id").as[String] -> node
  }.toMap

  def getValueById(key: String) = byId.get(key)

  def getUris(jsval: JsValue) = if (jsval.validate[JsArray].isSuccess) jsval.as[Seq[String]] else Seq(jsval.as[String])

  def mapValue[T](jsval: Option[JsValue])(implicit reads: Reads[T]) = mapValues(jsval).flatMap(_.headOption)

  def mapValues[T](jsval: Option[JsValue])(implicit reads: Reads[T]) = {
    try {
      jsval.map(getUris(_).flatMap(getValueById(_).flatMap(_.asOpt[T])))
    }
    catch {
      case t: Throwable => {
        logger.error(s"An error occurred while mapping values ${Json.prettyPrint(jsval.getOrElse(JsNull))}", t)
        None
      }
    }
  }
}
